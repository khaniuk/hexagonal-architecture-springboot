package com.drux.hexagonal.architecture.springboot.infrastructure.controllers;

import com.drux.hexagonal.architecture.springboot.common.exceptions.ServiceException;
import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/roles")
@AllArgsConstructor
public class RoleController {
    private final RoleService roleService;

    @GetMapping
    public ResponseEntity<List<RoleDto>> getRoles() {
        List<RoleDto> roles = roleService.getRoles();

        return ResponseEntity.of(Optional.of(roles));
    }

    @GetMapping("/{id}")
    public ResponseEntity<RoleDto> getRoleById(@PathVariable final Integer id) {
        RoleDto role = roleService.getRoleById(id);

        return ResponseEntity.of(Optional.of(role));
    }

    @PostMapping
    public ResponseEntity<RoleDto> createRole(@RequestBody final RoleDto roleDto) throws ServiceException {
        RoleDto newRoleDto = this.roleService.createRole(roleDto);

        return ResponseEntity.of(Optional.of(newRoleDto));
    }

    @DeleteMapping("/{id}")
    public void deleteRole(@PathVariable final Integer id) {
        roleService.deleteRole(id);
    }

}
