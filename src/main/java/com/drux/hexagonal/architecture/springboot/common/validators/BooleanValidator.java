package com.drux.hexagonal.architecture.springboot.common.validators;

import com.drux.hexagonal.architecture.springboot.common.validators.base.ValidatorServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class BooleanValidator extends ValidatorServiceImpl<Boolean> {

    public static BooleanValidator create() {
        return new BooleanValidator();
    }

    @Override
    public List<String> validate(Boolean value) {
        List<String> messages = new ArrayList<>();

        if (value == null) {
            messages.add("Value not valid");
        }
        return messages;
    }
}
