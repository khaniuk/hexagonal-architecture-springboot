package com.drux.hexagonal.architecture.springboot.application.services;

import com.drux.hexagonal.architecture.springboot.application.validators.RoleValidator;
import com.drux.hexagonal.architecture.springboot.common.exceptions.ServiceException;
import com.drux.hexagonal.architecture.springboot.common.utils.JsonUtil;
import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;
import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final JsonUtil jsonUtil = JsonUtil.create();
    private final GetRole getRole;
    private final CreateRole createRole;
    private final UpdateRole updateRole;
    private final DeleteRole deleteRole;

    @Override
    public List<RoleDto> getRoles() {
        List<Role> roles = getRole.getRoles();

        return jsonUtil.map(roles, List.class, RoleDto.class);
    }

    @Override
    public RoleDto getRoleById(Integer id) {
        Role role = getRole.getRoleById(id);

        return jsonUtil.map(role, RoleDto.class);
    }

    @Override
    public RoleDto createRole(RoleDto roleDto) throws ServiceException {
        List<String> messages = RoleValidator.create().validate(roleDto);

        if (!messages.isEmpty()) {
            throw new ServiceException(messages.get(0));
        }

        final Role role = jsonUtil.map(roleDto, Role.class);

        Role newRole = createRole.createRole(role);

        roleDto.setId(newRole.getId());

        return roleDto;
    }

    @Override
    public RoleDto updateRole(RoleDto roleDto) {
        final Role role = jsonUtil.map(roleDto, Role.class);

        var newRole = updateRole.updateRole(role);

        roleDto.setId(newRole.getId());

        return roleDto;
    }

    @Override
    public void deleteRole(Integer id) {
        deleteRole.deleteRole(id);
    }
}
