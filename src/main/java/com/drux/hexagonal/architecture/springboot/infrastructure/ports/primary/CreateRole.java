package com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;

public interface CreateRole {
    Role createRole(Role role);
}
