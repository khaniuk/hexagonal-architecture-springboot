package com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;

public interface UpdateRole {
    Role updateRole(Role role);
}
