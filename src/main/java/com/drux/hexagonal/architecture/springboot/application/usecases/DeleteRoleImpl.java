package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.DeleteRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DeleteRoleImpl implements DeleteRole {
    private final RoleRepository roleRepository;

    @Override
    public void deleteRole(Integer id) {
        roleRepository.delete(id);
    }
}
