package com.drux.hexagonal.architecture.springboot.application.validators;

import com.drux.hexagonal.architecture.springboot.common.validators.BooleanValidator;
import com.drux.hexagonal.architecture.springboot.common.validators.TextValidator;
import com.drux.hexagonal.architecture.springboot.common.validators.base.ValidatorServiceImpl;
import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;

import java.util.ArrayList;
import java.util.List;

public class RoleValidator extends ValidatorServiceImpl<RoleDto> {
    public static RoleValidator create() {
        return new RoleValidator();
    }

    @Override
    public List<String> validate(RoleDto roleDto) {
        List<String> messages = new ArrayList<>();

        messages.addAll(TextValidator.create().validate(roleDto.getDescription()));
        messages.addAll(BooleanValidator.create().validate(roleDto.getStatus()));

        return messages;
    }
}
