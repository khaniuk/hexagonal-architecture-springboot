package com.drux.hexagonal.architecture.springboot.infrastructure.repositories;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class InMemoryRoleRepositoryImpl implements RoleRepository {
    public static List<Role> roles = new ArrayList<>();

    @Override
    public List<Role> getAll() {
        return roles;
    }

    @Override
    public Role getById(Integer id) {
        List<Role> findRole = roles.stream().filter(role -> role.getId().equals(id)).toList();
        return findRole.get(0);
    }

    @Override
    public Role create(Role role) {
        if (roles.add(role))
            return role;

        return role;
    }

    @Override
    public Role update(Role newRole) {
        roles = roles.stream().filter(role -> !role.getId().equals(newRole.getId()))
                .collect(Collectors.toList());

        roles.add(newRole);

        return newRole;
    }

    @Override
    public void delete(Integer id) {
        roles = roles.stream().filter(role -> !role.getId().equals(id))
                .collect(Collectors.toList());
    }
}
