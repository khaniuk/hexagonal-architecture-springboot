package com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;

import java.util.List;

public interface GetRole {
    List<Role> getRoles();

    Role getRoleById(Integer id);
}
