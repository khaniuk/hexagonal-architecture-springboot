package com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary;

public interface DeleteRole {
    void deleteRole(Integer id);
}
