package com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary;

import com.drux.hexagonal.architecture.springboot.common.exceptions.ServiceException;
import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;

import java.util.List;

public interface RoleService {
    List<RoleDto> getRoles();

    RoleDto getRoleById(Integer id);

    RoleDto createRole(RoleDto role) throws ServiceException;

    RoleDto updateRole(RoleDto role);

    void deleteRole(Integer id);
}
