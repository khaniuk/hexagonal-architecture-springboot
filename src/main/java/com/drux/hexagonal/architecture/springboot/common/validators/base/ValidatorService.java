package com.drux.hexagonal.architecture.springboot.common.validators.base;

import java.util.List;

public interface ValidatorService<T> {
    List<String> validate(T var1);
}