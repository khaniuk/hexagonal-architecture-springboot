package com.drux.hexagonal.architecture.springboot.domain.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Role {
    Integer id;
    String description;
    Boolean status;
}
