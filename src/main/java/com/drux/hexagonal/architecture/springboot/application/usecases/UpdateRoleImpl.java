package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.UpdateRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class UpdateRoleImpl implements UpdateRole {
    private final RoleRepository roleRepository;

    @Override
    public Role updateRole(Role role) {
        return roleRepository.update(role);
    }
}
