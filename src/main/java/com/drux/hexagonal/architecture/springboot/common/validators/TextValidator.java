package com.drux.hexagonal.architecture.springboot.common.validators;

import com.drux.hexagonal.architecture.springboot.common.validators.base.ValidatorServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class TextValidator extends ValidatorServiceImpl<String> {

    public static TextValidator create() {
        return new TextValidator();
    }

    @Override
    public List<String> validate(String value) {
        List<String> messages = new ArrayList<>();

        if (value == null || value.isEmpty()) {
            messages.add("Text value not valid");
        }
        return messages;
    }
}
