package com.drux.hexagonal.architecture.springboot.common.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class JsonUtil {
    private final GsonBuilder builder = (new GsonBuilder()).setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    public JsonUtil() {
    }

    public GsonBuilder getBuilder() {
        return this.builder;
    }

    public static JsonUtil create() {
        return new JsonUtil();
    }

    public String toJson(Object object) {
        return this.builder.create().toJson(object);
    }

    public String toJson(Object object, Type rawType, Type... typeArguments) {
        TypeToken<?> vTypeToken = TypeToken.getParameterized(rawType, typeArguments);
        return this.builder.create().toJson(object, vTypeToken.getType());
    }

    public <T> T fromJson(String json, Class<T> clase) {
        return this.builder.create().fromJson(json, clase);
    }

    public <T> T fromJson(String json, Type rawType, Type... typeArguments) {
        TypeToken<?> vTypeToken = TypeToken.getParameterized(rawType, typeArguments);
        return this.builder.create().fromJson(json, vTypeToken.getType());
    }

    public <T> T map(Object object, Class<T> clase) {
        String vJson = this.toJson(object);
        return this.fromJson(vJson, clase);
    }

    public <T> T map(Object object, Type rawType, Type... typeArguments) {
        String vJson = this.toJson(object, rawType, typeArguments);
        return this.fromJson(vJson, rawType, typeArguments);
    }
}