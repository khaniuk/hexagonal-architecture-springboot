package com.drux.hexagonal.architecture.springboot.domain.dtos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {
    Integer id;
    String description;
    Boolean status;
}
