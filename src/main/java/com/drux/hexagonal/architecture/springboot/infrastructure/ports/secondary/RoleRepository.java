package com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(Integer id);

    Role create(Role role);

    Role update(Role role);

    void delete(Integer id);
}
