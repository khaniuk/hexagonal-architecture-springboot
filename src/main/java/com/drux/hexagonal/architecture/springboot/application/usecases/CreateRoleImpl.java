package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.CreateRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CreateRoleImpl implements CreateRole {
    private final RoleRepository roleRepository;

    @Override
    public Role createRole(Role role) {
        return roleRepository.create(role);
    }
}
