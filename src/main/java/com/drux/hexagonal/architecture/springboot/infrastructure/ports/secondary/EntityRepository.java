package com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary;

import java.util.List;

public interface EntityRepository {
    <T> List<T> getAll();

    <T> T getById(Object id);

    <T> T save(T entity);

    <T> T update(T entity);

    void delete(Object id);
}
