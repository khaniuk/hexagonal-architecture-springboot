package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.CreateRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CreateRoleImplTest {
    private final RoleRepository roleRepository = mock(RoleRepository.class);

    private final CreateRole createRole = new CreateRoleImpl(roleRepository);

    @Test
    void shouldCallRepositoryToCreateRole() {
        Role role = Role.builder()
                .id(1)
                .description("Admin")
                .status(true)
                .build();

        createRole.createRole(role);

        verify(roleRepository).create(role);
    }
}