package com.drux.hexagonal.architecture.springboot.infrastructure.repositories;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InMemoryRoleRepositoryImplTest {
    private final RoleRepository roleRepository = new InMemoryRoleRepositoryImpl();

    @AfterEach
    public void restoreDataBase() {
        InMemoryRoleRepositoryImpl.roles = new ArrayList<>();
    }

    @Test
    void shouldGetRoles() {
        Role role1 = Role.builder().id(1).build();
        Role role2 = Role.builder().id(2).build();

        roleRepository.create(role1);
        roleRepository.create(role2);

        List<Role> roles = roleRepository.getAll();

        assertEquals(2, roles.size());
    }

    @Test
    void shouldGetRoleById() {
        Integer roleId = 1;

        roleRepository.create(Role.builder()
                .id(roleId)
                .build());

        Role role = roleRepository.getById(roleId);

        assertEquals(roleId, role.getId());
    }

    @Test
    void shouldCreateRole() {
        Role role = Role.builder().build();

        roleRepository.create(role);

        assertEquals(1, InMemoryRoleRepositoryImpl.roles.size());
    }

    @Test
    void shouldUpdateRole() {
        Role role = Role.builder()
                .id(1)
                .description("Admin")
                .status(true)
                .build();

        Role roleCreate = roleRepository.create(role);

        role.setDescription("Manager");
        Role roleUpdate = roleRepository.update(role);

        assertEquals(roleCreate.getId(), roleUpdate.getId());
    }

    @Test
    void shouldDeleteRole() {
        final int id = 1;

        Role role = Role.builder()
                .id(id)
                .build();

        roleRepository.create(role);

        roleRepository.delete(id);

        assertEquals(0, InMemoryRoleRepositoryImpl.roles.size());
    }
}