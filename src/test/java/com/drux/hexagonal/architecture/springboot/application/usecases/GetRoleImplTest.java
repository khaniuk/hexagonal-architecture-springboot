package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.GetRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class GetRoleImplTest {
    private final RoleRepository roleRepository = mock(RoleRepository.class);

    private final GetRole getRole = new GetRoleImpl(roleRepository);

    @Test
    void shouldCallRepositoryToGetRoles() {
        getRole.getRoles();

        verify(roleRepository).getAll();
    }

    @Test
    void shouldCallRepositoryToGetRoleById() {
        final Integer id = 1;

        getRole.getRoleById(id);

        verify(roleRepository).getById(id);
    }
}