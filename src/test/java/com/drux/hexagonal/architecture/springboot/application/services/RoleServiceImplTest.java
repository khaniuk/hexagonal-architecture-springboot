package com.drux.hexagonal.architecture.springboot.application.services;

import com.drux.hexagonal.architecture.springboot.application.usecases.CreateRoleImpl;
import com.drux.hexagonal.architecture.springboot.application.usecases.DeleteRoleImpl;
import com.drux.hexagonal.architecture.springboot.application.usecases.GetRoleImpl;
import com.drux.hexagonal.architecture.springboot.application.usecases.UpdateRoleImpl;
import com.drux.hexagonal.architecture.springboot.common.utils.JsonUtil;
import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;
import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.CreateRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.DeleteRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.GetRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.UpdateRole;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

class RoleServiceImplTest {
    private final JsonUtil jsonUtil = mock(JsonUtil.class);
    private final GetRole getRole = Mockito.mock(GetRoleImpl.class);
    private final CreateRole createRole = Mockito.mock(CreateRoleImpl.class);
    private final UpdateRole updateRole = Mockito.mock(UpdateRoleImpl.class);
    private final DeleteRole deleteRole = Mockito.mock(DeleteRoleImpl.class);

    private final RoleServiceImpl roleService = new RoleServiceImpl(getRole, createRole, updateRole, deleteRole);

    @Test
    void shouldCallRepositoryToGetRoles() {
        roleService.getRoles();

        verify(getRole).getRoles();
    }

    @Test
    void shouldCallRepositoryToCreateRole() throws Exception {
        RoleDto roleDto = RoleDto.builder()
                .id(1)
                .description("Admin")
                .status(true)
                .build();

        Role role = Role.builder()
                .id(roleDto.getId())
                .description(roleDto.getDescription())
                .status(roleDto.getStatus())
                .build();

        when(jsonUtil.map(role, RoleDto.class)).thenReturn(roleDto);
        when(jsonUtil.map(roleDto, Role.class)).thenReturn(role);

        roleService.createRole(roleDto);

        verify(createRole);
    }

    @Test
    void shouldCallRepositoryToDeleteRole() {
        final int id = 1;

        roleService.deleteRole(id);

        verify(deleteRole).deleteRole(id);
    }
}