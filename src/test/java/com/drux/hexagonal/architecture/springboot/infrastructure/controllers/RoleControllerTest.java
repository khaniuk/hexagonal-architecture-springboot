package com.drux.hexagonal.architecture.springboot.infrastructure.controllers;

import com.drux.hexagonal.architecture.springboot.application.services.RoleServiceImpl;
import com.drux.hexagonal.architecture.springboot.common.exceptions.ServiceException;
import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class RoleControllerTest {
    private final RoleServiceImpl roleService = mock(RoleServiceImpl.class);

    private final RoleController roleController = new RoleController(roleService);

    @Test
    void shouldCallServiceToGetRoles() {
        roleController.getRoles();

        verify(roleService).getRoles();
    }

    @Test
    void shouldCallServiceToGetRoleById() throws ServiceException {
        final int id = 1;
        RoleDto roleDto = RoleDto.builder()
                .id(id)
                .description("Admin")
                .status(true)
                .build();

        roleController.createRole(roleDto);

        roleController.getRoleById(id);

        verify(roleService).getRoleById(id);
    }

    @Test
    void shouldCallServiceToCreateRole() throws ServiceException {
        RoleDto roleDto = RoleDto.builder()
                .id(1)
                .description("Admin")
                .status(true)
                .build();

        roleController.createRole(roleDto);

        verify(roleService).createRole(roleDto);
    }

    @Test
    void shouldCallServiceToDeleteRole() {
        final int id = 1;

        roleController.deleteRole(id);

        verify(roleService).deleteRole(id);
    }
}