package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.domain.entities.Role;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.UpdateRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class UpdateRoleImplTest {
    private final RoleRepository roleRepository = mock(RoleRepository.class);

    private final UpdateRole updateRole = new UpdateRoleImpl(roleRepository);

    @Test
    void shouldCallRepositoryToUpdateRole() {
        Role role = Role.builder()
                .id(1)
                .description("Admin")
                .status(true)
                .build();

        updateRole.updateRole(role);

        verify(roleRepository).update(role);
    }
}