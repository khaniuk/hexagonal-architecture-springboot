package com.drux.hexagonal.architecture.springboot.application.usecases;

import com.drux.hexagonal.architecture.springboot.infrastructure.ports.primary.DeleteRole;
import com.drux.hexagonal.architecture.springboot.infrastructure.ports.secondary.RoleRepository;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class DeleteRoleImplTest {
    private final RoleRepository roleRepository = mock(RoleRepository.class);

    private final DeleteRole deleteRole = new DeleteRoleImpl(roleRepository);

    @Test
    void shouldCallRepositoryToDeleteRole() {
        final int id = 1;

        deleteRole.deleteRole(id);

        verify(roleRepository).delete(id);
    }
}