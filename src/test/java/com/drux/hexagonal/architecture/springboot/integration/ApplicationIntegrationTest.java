package com.drux.hexagonal.architecture.springboot.integration;

import com.drux.hexagonal.architecture.springboot.domain.dtos.RoleDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldGetRoles() throws Exception {
        final Integer roleId = 1;
        RoleDto roleDto = RoleDto.builder()
                .id(roleId)
                .description("Test")
                .status(true)
                .build();

        this.mockMvc.perform(post("/roles")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(roleDto)));

        this.mockMvc.perform(get("/roles")
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldCreateRole() throws Exception {
        final Integer roleId = 1;
        RoleDto roleDto = RoleDto.builder()
                .id(roleId)
                .description("Test")
                .status(true)
                .build();

        this.mockMvc.perform(post("/roles")
                        .contentType("application/json")
                        .content(this.objectMapper.writeValueAsString(roleDto)))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteRole() throws Exception {
        final Integer roleId = 1;
        RoleDto roleDto = RoleDto.builder()
                .id(roleId)
                .description("Test")
                .status(true)
                .build();

        this.mockMvc.perform(post("/roles")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(roleDto)));

        this.mockMvc.perform(delete("/roles/{id}", roleId)
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }
}
