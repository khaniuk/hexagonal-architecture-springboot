# Hexagonal Architecture SpringBoot

## About Project

Project to manage user roles, using a hexagonal architecture structure (also known as ports and adapters).

## Hexagonal Architecture

The hexagonal architecture, also known as ports and adapters, is organized into several layers to achieve clear separation of concerns and increased modularity. Here are the main layers:

 - Application Layer (Core): At the heart of the hexagonal architecture is the application layer, which contains the core business logic. This layer is not dependent on any other and defines ports that represent interfaces for data input and output.

 - Ports: Ports are interfaces defined in the application layer that represent how data is input and extracted. There are two types of ports: input ports for data input into the system and output ports for data output from the system.

 - Adapters: Adapters implement the interfaces defined in the ports. These adapters allow the application to interact with the external world. They can be input adapters (to handle user requests, for example, web controllers) or output adapters (to interact with databases, external services, etc.).

 - Infrastructure Layer: This layer contains components that are specific to the infrastructure, such as databases, external services, frameworks, etc. Output adapters in the application layer interact with this layer to persist data or communicate with external services.

The key advantage of this architecture is that the business logic is not directly coupled to the infrastructure. Ports and adapters provide flexibility and easy replacement of components without affecting the core logic of the application. This facilitates unit testing, scalability, and code maintenance.
